'use strict';
/**
 * @param {object} $inject - Dependencies
 * @param {*} $inject.redisClient - instance of redis.createClient
 */
function redisService_injector($inject) {
  const { redisClient } = $inject;

  const service = {
    cache,
  };

  const MAKE_PRIVATE = { enumerable : false, writable: true };
  Object.defineProperties(service, {
    add : { ...MAKE_PRIVATE, value : add },
    remove : { ...MAKE_PRIVATE, value : remove },
    get : { ...MAKE_PRIVATE, value : get },
    getTTL : { ...MAKE_PRIVATE, value : getTTL},
  });

  return service;

  /**
   * This generates a new middleware function used for caching
   * @access public
   * @param {object} options 
   * @param {number} options.ttl - the time to live, in seconds
   * @returns {function} new middleware function
   */
  function cache(options = {}) {
    // Return a middleware function that can be used for setting up cache
    return async function cachingMiddleware(req, res, next) {
      if (req.method !== 'GET') {
        res.set('Cache-control', 'no-store');
        return next(); //NEVER cache PUT / POST etc.
      }
      // We want originalUrl but without query params first
      const customKey = `${req.baseUrl}${req.path}`;
      // Then sort the keys and append them
      const queryString = Object.keys(req.query).sort().map(key => {
        return `${key.toLowerCase()}=${req.query[key]}`
      }).join('&');
      // That gives us a unique cache key
      const cacheKey = `${customKey}?${queryString}`;
      const ttl = options.ttl || 60; // in seconds. Default to 1 minute

      let value;
      let timeRemaining;
      try {
        value = await service.get(cacheKey);
        timeRemaining = await service.getTTL(cacheKey);
      } catch (err) {
        // logger.error(`Failed to communicate with cache: ${err}`);
        return next(); // don't error on cache failure
      }
      if (value) {
        res.set('X-Cache-Info', 'from-cache'); // Custom header for debugging
        res.set('Cache-control', `public, max-age=${timeRemaining}`);
        // logger.debug(`Fetched data from cache using key '${cacheKey}'.`);
        return res.json(value);
      }
      // I don't love this, but it makes things easy
      res._jsonOriginal = res.json;
      res.json = function proxiedJson(data) {
        service.add(cacheKey, data, ttl);
        res.set('X-Cache-Info', 'cached'); // Custom header for debugging
        res.set('Cache-control', `public, max-age=${ttl}`);
        return res._jsonOriginal(data);
      }
      return next();
  
    }
  }

  /**
   * Gets remaining time to live for an item in the cache. Useful for
   * generating max-age headers
   * @access private
   * @param {string} key 
   */
  function getTTL(key) {
    return new Promise((resolve, reject) => {
      redisClient.ttl(key, (err, timeRemaining) => {
        if (err) {
          return resolve(0);
        }
        if (timeRemaining < 0 || typeof timeRemaining !== 'number') { // redis backward compat
          return resolve(0);
        }
        return resolve(timeRemaining);
      });
    });
  }

  /**
   * Retrieves an item from the cache
   * @access private
   * @param {string} key
   * @returns {Promise<object|null|Error>} 
   */
  function get(key) {
    return new Promise((resolve, reject) => {
      redisClient.get(key, function(err, value) {
        if (err) {
          return reject(err);
        }
        let safeValue = value;
        if (value !== null) {
          safeValue = JSON.parse(value);
        }
        return resolve(safeValue);
      });
    });
  }

  /**
   * Addes an item to the cache with the set ttl
   * @access private
   * @param {string} key 
   * @param {string|number|object}} value 
   * @param {number} ttl - Time to live
   * @returns {void}
   */
  function add(key, value, ttl = 60) {
    let safeValue = value;
    if (typeof value !== 'string') {
      safeValue = JSON.stringify(value);
    }
    return redisClient.set(key, safeValue, 'EX', ttl);
  }
  
  /**
   * Removes an item from the cache. Not currently used
   * @acces private
   * @param {string} key 
   * @returns {void}
   */
  function remove(key) {
    return redisClient.del(key);
  }
}

module.exports = redisService_injector;
