const $inject = {
  redisClient : require('../../startup/cache').getInstance(),
  logger : require('../logger')
};

const cacheService = require('./cache.service')($inject);

module.exports = cacheService;