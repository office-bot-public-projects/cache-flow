'use strict';
/**
 * @param {object} $inject - Dependencies
 * @param {*} $inject.redis - Redis npm library
 */
function cacheStartup_injector($inject) {
  const { redis } = $inject;

  let client;

  const startup = {
    start,
    getInstance,
  };

  return startup;

  /**
   * @access public
   * @returns {redisClient|null}
   */
  function getInstance() {
    return client;
  }

  /**
   * @access public
   * @param {*} handlers - optional handlers
   */
  function start(handlers = {}) {
    return new Promise((resolve, reject) => {
      client = redis.createClient();
      client.on('error', function redisError(err) {
        return reject(err);
      });
      client.on('ready', function redisReady() {
        return resolve(client);
      });
      const allowedEvents = [
        'warning',
        'reconnecting',
        'end'
      ];
      allowedEvents.forEach(eventName => {
        if (typeof handlers[eventName] === 'function') {
          client.on(eventName, handlers[eventName]);
        }
      });
    });  
  }
}

module.exports = cacheStartup_injector;
