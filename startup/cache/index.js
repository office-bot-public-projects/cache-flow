const $inject = {
  redis : require('redis'),
}

const cacheStartup = require('./cache.startup')($inject);
module.exports = cacheStartup;
